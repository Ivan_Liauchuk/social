Social
------
This is a demo project for the university studies. Project is build with the help of [Yii2](http://www.yiiframework.com/)  framework.


Installation
------------

This article describe installation of web application using docker.
This installation way does not require pre-installed software (such as web-server, PHP, MySQL etc.) - just do next steps!

#### Windows

1. Install [Vagrant](https://www.vagrantup.com/downloads.html)
2. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
3. Reboot your machine
4. Open terminal (`cmd.exe`)
5. Run `vagrant plugin update`
6. **Change directory to project root**
7. Copy `vagrant-local.example.yml` to `vagrant-local.yml` inside folder `vagrant/config`. Generate `github_token` to `vagrant-local.yml` on [GitHub](https://github.com)
7. In terminal(`cmd.exe`) run `vagrant up`

#### Linux/Unix

1. Install [Docker](https://www.docker.com/community-edition#/download)
2. Reboot your machine
3. Copy `.env.template` to `.env` inside folder `docker`. Change `.env` settings, if required
4. Open terminal
5. **Change directory to project root**
6. Go inside docker folder `$ cd docker`
7. Run `$ docker-compose build`
8. Run `$ docker-compose up -d`
9. Add new line `127.0.0.1 social.test` to `/etc/hosts`

* If you want to use [Xdebug](https://xdebug.org/), you can check this [article](https://gist.github.com/manuelselbach/8a214ae012964b1d49d9fb019f5f5d7b).

#####Default MySql credentials:
 
* username: `social`
* password: `social`
* port: `32679`


Launching after installation
----------------------------
#### Windows

1. Open terminal (`cmd.exe`)
2. Change directory to project root
3. Run `vagrant up`
4. If you need stop vagrant, in terminal(`cmd.exe`) run `vagrant halt`

#### Linux/Unix

1. Open terminal
2. Change directory to project root
3. Go inside docker folder `$ cd docker`
4. Run `$ docker-compose up -d`