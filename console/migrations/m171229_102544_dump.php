<?php

use yii\db\Migration;
use common\models\About;
use common\models\Appointments;
use common\models\Complains;
use common\models\Departments;
use common\models\Documents;
use common\models\News;
use common\models\Services;
use common\models\Users;

/**
 * Class m171229_102544_dump
 */
class m171229_102544_dump extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute(file_get_contents('dump.sql'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(About::tableName());
        $this->dropTable(Appointments::tableName());
        $this->dropTable(Complains::tableName());
        $this->dropTable(Departments::tableName());
        $this->dropTable(Documents::tableName());
        $this->dropTable(News::tableName());
        $this->dropTable(Services::tableName());
        $this->dropTable(Users::tableName());
        $this->dropTable('schedule');
    }

}
