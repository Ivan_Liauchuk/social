<?php

use common\models\Appointments;
use common\widgets\Alert;
use common\widgets\schedule\ScheduleModel;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $form ActiveForm
 */

$model = new Appointments();

Pjax::begin([
    'enablePushState' => false,
]);
?>
    <div class="form-wrapper">
        <h2>
            Запись на прием
        </h2>
        <?= Alert::widget() ?>
        <?php
        $form = ActiveForm::begin([
            'action' => '/site/add-appointment',
            'options' => [
                'data' => [
                    'pjax' => true
                ]
            ]
        ]);
        ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'date')->widget(DatePicker::class, [
            'pluginOptions' => [
                'autoclose' => true,
                'daysOfWeekDisabled' => ScheduleModel::getModel()->getDisabledDaysOfWeek(),
            ],
            'removeButton' => false,
            'options' => [
                'data' => [
                    'url' => Url::to(['site/get-time-dropdown']),
                ]
            ]
        ]) ?>
        <?= $form->field($model, 'time')->dropDownList([], [
            'disabled' => true,
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Записаться', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<?php
Pjax::end();