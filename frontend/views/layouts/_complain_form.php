<?php

use common\models\Complains;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $form ActiveForm
 */

$model = new Complains();

Pjax::begin([
    'enablePushState' => false,
]);
?>

    <div class="form-wrapper">
        <h2>
            Отправить обращение
        </h2>
        <?= Alert::widget() ?>

        <?php $form = ActiveForm::begin([
            'id' => 'complain-form',
            'action' => '/site/add-complain',
            'options' => [
                'data' => [
                    'pjax' => true
                ]
            ]
        ]); ?>

        <?= $form->field($model, 'isAnonymously')->checkbox() ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput() ?>
        <?= $form->field($model, 'email')->textInput() ?>
        <?= $form->field($model, 'complain')->textarea(['rows' => 4]) ?>

        <div class="form-group">
            <?= Html::submitButton('Добавить жалобу', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<?php
Pjax::end();