<?php


use yii\web\View;

/**
 * @var $this View
 * @var $shortPhone string
 */
?>

<div>
    <p class="font-24 text-center mb-20 hidden-xs">
        <i class="btn-icon fa fa-phone "></i> <?= $shortPhone ?>
    </p>
</div>

<div class="mb-45">
    <?= $this->render('_appointment_form') ?>
</div>

<div>
    <?= $this->render('_complain_form') ?>
</div>