<?php

use common\widgets\Alert;
use frontend\assets\AppAsset;
use frontend\controllers\SiteController;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $content string
 */

AppAsset::register($this);
/** @var SiteController $controller */
$controller = $this->context;
$about = $controller->about;
$workingHours = $controller->workingHours;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="/img/logo.png" type="image/x-icon">

    <!-- Web Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Pacifico%7CSource+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;amp;subset=latin-ext,vietnamese"
          rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    -->
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="preloaderKDZ"></div>
<div class="yolo-site">
    <header class="header yolo-header-style-6">
        <div class="yolo-top-bar">
            <div class="container">
                <div class="row">
                    <div class="top-sidebar top-bar-left col-md-4">
                        <aside id="text-11" class="widget widget_text">
                            <div class="textwidget">
                                <div>Центр социального обслуживания населения</div>
                            </div>
                        </aside>
                    </div>
                    <div class="top-sidebar top-bar-right col-md-8">
                        <aside id="text-6" class="widget widget_text">
                            <div class="textwidget">
                                <i class="fa fa-home"></i> <?= $about->address ?>
                                <i style="margin-left: 15px" class="fa fa-phone"></i> <?= $about->phone ?>
                                <i style="margin-left: 15px" class="fa fa-clock-o"></i> <?= $workingHours ?>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>

        <div class="mobile-menu">
            <div class="col-3 text-left"><a href="#primary-menu"><i class="fa fa-bars"></i></a></div>
            <div class="col-3 text-center">
                <div class="logo">
                    <h1>
                        <a href="/">
                            <img src="/img/logo.png" alt="logo"/>
                        </a>
                    </h1>
                </div>
            </div>
            <div class="col-3 text-center">
                <p class="font-24">
                    <i class="btn-icon fa fa-phone "></i> <?= $about->phone ?>
                </p>
                <p class="font-24">
                    <i class="btn-icon fa fa-phone "></i> <?= $about->short_phone ?>
                </p>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="main-nav-wrapper">
                    <div class="header-logo">
                        <h1>
                            <a href="/">
                                <img src="/img/logo.png" alt="logo"/>
                            </a>
                        </h1>
                    </div>
                    <div class="header-left">
                        <nav id="primary-menu" class="main-nav">
                            <?php
                            $menuItems = [
                                [
                                    'label' => 'Главная',
                                    'url' => ['site/index'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Услуги',
                                    'url' => ['site/services'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Правовая база',
                                    'url' => ['site/documents'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'О нас',
                                    'url' => ['site/about'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Структура центра',
                                    'url' => ['site/departments'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                            ];

                            echo Nav::widget([
                                'options' => ['class' => 'nav'],
                                'items' => $menuItems,
                            ]);
                            ?>
                        </nav>

                    </div>
                    <div class="header-right">
                        <div class="form-input">
                            <input id="search-field" style="text" placeholder="Поиск"
                                   data-url="<?= Url::to(['site/search-news']) ?>">
                            <a href="#">
                                <i class="fa fa-search"></i>
                            </a>
                        </div>

                        <div id="search-result" class="hidden">
                            <div class="loading-container text-center hidden">
                                <img src="/img/loading.gif">
                            </div>
                            <div class="results"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </header>

    <div id="wrapper">
        <?= Alert::widget() ?>
        <div class="container mb">
            <div class="col-md-9">
                <?= $content ?>
            </div>
            <div class="col-md-3">
                <?= $this->render('_left_sub_menu', [
                    'shortPhone' => $about->short_phone
                ]) ?>
            </div>
        </div>
    </div>
</div>


<div class="div-box">
    <footer id="yolo-footer-wrapper">
        <div class="yolo-footer-wrapper footer-1 footer-7">
            <div class="footer-bottom">
                <div class="container">
                    <p class="copyright">Центр социального обслуживания населения <?= date('Y') ?></p>
                </div>
            </div>
        </div>
    </footer>
</div>


<div class="popup-wrapper"></div>

<div class="click-back-top-body">
    <button type="button" class="sn-btn sn-btn-style-17 sn-back-to-top fixed-right-bottom">
        <i class="btn-icon fa fa-angle-up"></i>
    </button>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
