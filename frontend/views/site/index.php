<?php

use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var $this View
 * @var $newsDataProvider ActiveDataProvider
 */

$this->title = 'Центр социального обслуживания населения';
?>
<div class="site-index">
    <h1>
        Наши новости
    </h1>

    <?= $this->render('_news_list_view', [
        'newsDataProvider' => $newsDataProvider
    ]) ?>
</div>
