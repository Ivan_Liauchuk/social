<?php

use common\models\News;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model News
 */

$this->title = $model->title;
?>
<div class="site-new">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <p>
        <?= $model->text ?>
    </p>

    <?= Html::a('Назад', ['/'], [
        'class' => 'btn btn-default'
    ]) ?>
</div>