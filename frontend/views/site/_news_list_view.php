<?php

use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var $this View
 * @var $newsDataProvider ActiveDataProvider
 */

echo ListView::widget([
    'layout' => "{items}\n{pager}",
    'dataProvider' => $newsDataProvider,
    'itemView' => '_new_list_view_item',
]);