<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var $this View
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Структура центра';
?>
<div class="site-index">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <?= ListView::widget([
        'layout' => "{items}\n{pager}",
        'dataProvider' => $dataProvider,
        'itemView' => '_department_list_view_item',
    ]) ?>
</div>
