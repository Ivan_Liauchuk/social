<?php

use common\models\DocumentsSearch;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var $this View
 * @var $dataProvider DocumentsSearch
 */

$this->title = 'Правовая база';
?>
<div class="site-index">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <p>
        <b>
            В своей деятельности Государственное учреждение «Любанский районный территориальный центр социального
            обслуживания населения» руководствуется следующими нормативно-правовыми документами:
        </b>
    </p>

    <div class="mt-20">

        <?= ListView::widget([
            'layout' => "{items}\n{pager}",
            'dataProvider' => $dataProvider,
            'itemView' => '_documents_list_view_item',
        ]) ?>
    </div>
</div>

