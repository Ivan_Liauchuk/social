<?php

use common\models\About;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model About
 */

$this->title = 'О нас';
$googleApiKey = Yii::$app->params['googleApiKey'];
?>
<div class="site-about">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <p class="mb-20">
        Уважаемые посетители!
        Мы всегда рады диалогу с вами. Присылайте свои обращения, пожелания и предложения. Мы не оставим их без
        внимания.
    </p>
    <div class="row">
        <div class="col-md-6">
            <p>
                <b>
                    <?= $model->getAttributeLabel('address') ?>:
                </b>
                <?= $model->address ?>
            </p>
            <p>
                <b>
                    <?= $model->getAttributeLabel('phone') ?>:
                </b>
                <?= $model->phone ?>
            </p>
            <p>
                <b>
                    <?= $model->getAttributeLabel('short_phone') ?>:
                </b>
                <?= $model->short_phone ?>
            </p>
            <p>
                <b>
                    <?= $model->getAttributeLabel('email') ?>:
                </b>
                <?= $model->email ?>
            </p>
            <p><?= $model->info ?></p>
        </div>
        <div class="col-md-6">
            <div class="uk-width-1-1">
                <iframe
                        width="100%"
                        height="300"
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?key=<?= $googleApiKey ?>&q=<?= $model->address ?>"
                        allowfullscreen>
                </iframe>
            </div>
        </div>
    </div>
</div>
