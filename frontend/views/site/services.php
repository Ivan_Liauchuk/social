<?php

use common\models\Services;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Services
 */

$this->title = 'Услуги';
?>
<div class="site-index">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <div>
        <?= $model->text ?>
    </div>
</div>
