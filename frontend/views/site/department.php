<?php

use common\models\Departments;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Departments
 */

$this->title = $model->title;
?>
<div class="site-new">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <p>
        <?= $model->info ?>
    </p>

    <?= Html::a('Назад', ['site/departments'], [
        'class' => 'btn btn-default'
    ]) ?>
</div>