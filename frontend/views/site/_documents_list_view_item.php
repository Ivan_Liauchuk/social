<?php

use common\models\Documents;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Documents;
 */

?>

<p>
    <?= Html::a($model->title, $model->getUrl(), [
        'download' => true,
    ]) ?>
</p>
