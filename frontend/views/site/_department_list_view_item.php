<?php

use common\models\Departments;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Departments
 */

?>

<div class="mb-20">
    <p>
        <b>
            <?= Html::a($model->title, [
                'site/department',
                'id' => $model->id,
            ]) ?>
        </b>
    </p>
</div>
