<?php

use common\models\News;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $model News
 */

?>

<div>
    <p>
        <b>
            <?= $model->title ?>
        </b>
    </p>
    <div class="mt-20">
        <?= StringHelper::truncate($model->text, 400) ?>
    </div>
</div>
<p>
    <?= Html::a('Читать дальше', [
        'site/new',
        'id' => $model->id,
    ], [
        'class' => 'btn btn-default'
    ]) ?>
</p>
