<?php

namespace frontend\controllers;

use common\exceptions\NotFoundModelException;
use common\helpers\AlertHelper;
use common\models\About;
use common\models\Appointments;
use common\models\Complains;
use common\models\Departments;
use common\models\DepartmentsSearch;
use common\models\DocumentsSearch;
use common\models\News;
use common\models\NewsSearch;
use common\models\Services;
use common\widgets\schedule\Schedule;
use common\widgets\schedule\ScheduleModel;
use common\widgets\schedule\ScheduleStringHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\captcha\CaptchaAction;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ErrorAction;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /** @var About */
    public $about;
    /** @var string */
    public $workingHours;

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
            'captcha' => [
                'class' => CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'test-me' : null,
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        $this->about = About::getModel();
        $scheduleStringHelper = new ScheduleStringHelper(ScheduleModel::getModel());
        $this->workingHours = $scheduleStringHelper->asString();

        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $newsDataProvider = $searchModel->search();

        return $this->render('index', [
            'newsDataProvider' => $newsDataProvider
        ]);
    }

    /**
     * Displays news content.
     *
     * @param int $id
     * @return string
     * @throws NotFoundModelException
     */
    public function actionNew(int $id): string
    {
        $model = News::findOne($id);
        if (!$model) {
            throw new NotFoundModelException('Не удалось найти новость');
        }

        return $this->render('new', [
            'model' => $model
        ]);
    }

    /**
     * @return string
     */
    public function actionServices(): string
    {
        return $this->render('services', [
            'model' => Services::getService()
        ]);
    }

    /**
     * @return string
     */
    public function actionDocuments(): string
    {
        $searchModel = new DocumentsSearch();

        return $this->render('documents', [
            'dataProvider' => $searchModel->search()
        ]);
    }

    /**
     * @return string
     */
    public function actionAddComplain(): string
    {
        $model = new Complains();
        $model->load(Yii::$app->request->post());
        if ($model->save()) {
            AlertHelper::success('Жалоба успешно добавлена');
        } else {
            AlertHelper::danger('При сохранении жалобы произошла ошибка');
        }

        return $this->renderAjax('//layouts/_complain_form');
    }

    /**
     * @return string
     */
    public function actionAddAppointment(): string
    {
        $model = new Appointments();
        $model->load(Yii::$app->request->post());
        if ($model->save()) {
            AlertHelper::success('Заявка успешно обработана');
        } else {
            AlertHelper::danger('При обработки заявки произошла ошибка');
        }

        return $this->renderAjax('//layouts/_appointment_form');
    }

    /**
     * @return string
     */
    public function actionAbout(): string
    {
        return $this->render('about', [
            'model' => About::getModel()
        ]);
    }

    /**
     * @return string
     */
    public function actionDepartments(): string
    {
        $searchModel = new DepartmentsSearch();
        $dataProvider = $searchModel->search();

        return $this->render('departments', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param int $id
     * @return string
     * @throws NotFoundModelException
     */
    public function actionDepartment(int $id): string
    {
        $model = Departments::findOne($id);
        if (!$model) {
            throw new NotFoundModelException('Не удалось найти новость');
        }

        return $this->render('department', [
            'model' => $model
        ]);
    }

    /**
     * @throws InvalidConfigException
     */
    public function actionGetTimeDropdown(): string
    {
        $date = Yii::$app->request->post('date');
        $date = Yii::$app->formatter->asDate($date, 'php:Y-m-d');

        $excludedTime = Appointments::find()
            ->where([
                'date' => $date,
            ])
            ->select('time')
            ->column();

        return Schedule::getDropDownWithAvailableTime($date, $excludedTime);
    }

    /**
     * @return string
     */
    public function actionSearchNews(): string
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->searchByText(Yii::$app->request->post('search'));

        return $this->renderAjax('_news_list_view', [
            'newsDataProvider' => $dataProvider
        ]);
    }

}
