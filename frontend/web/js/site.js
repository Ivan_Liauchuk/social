$(function () {
    $(document).on('change', 'input#complains-isanonymously', function () {
        let isChecked = $(this).prop('checked');
        let form = $('form#complain-form');
        if (isChecked) {
            form.addClass('anonymous');
        } else {
            form.removeClass('anonymous');
        }
    });

    $(document).on('pjax:start', function (e) {
        $(e.relatedTarget).html('<div class="loading"><img src="/img/loading.gif"/></div>');
    });

    $(document).on('change', 'input#appointments-date', function () {
        $.ajax({
            url: $(this).data('url'),
            method: 'POST',
            data: {
                date: $(this).val()
            }
        }).done(function (html) {
            let select = $('#appointments-time');
            select.html($(html)[0].innerHTML);
            select.removeAttr('disabled');
        });
    });

    let searchResultContainer = $('#search-result');
    let searchResultBlock = searchResultContainer.find('.results');
    let searchLoadingContainer = searchResultContainer.find('.loading-container');


    $(document).click(function (e) {
        if (event.target.id === 'search-field') {
            if (!$('input#search-field').hasClass('focused')) {
                $('input#search-field').addClass('focused');
                searchResultContainer.removeClass('hidden');
            }
        } else {
            $('input#search-field').removeClass('focused');
            searchResultContainer.addClass('hidden');
        }
    });

    $(document).on('keyup', 'input#search-field', function () {
        let content = $(this).val();
        searchResultBlock.html('');
        if (content.length > 2) {
            searchLoadingContainer.removeClass('hidden');
            $.ajax({
                url: $(this).data('url'),
                method: 'POST',
                data: {
                    search: $(this).val()
                }
            }).done(function (html) {
                searchLoadingContainer.addClass('hidden');
                searchResultBlock.html(html);
            });
        } else {
            searchLoadingContainer.addClass('hidden');
        }
    });

});
