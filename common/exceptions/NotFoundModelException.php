<?php

namespace common\exceptions;

use yii\base\Exception;

/**
 * Class NotFoundModelException
 * @package common\exceptionsl
 */
class NotFoundModelException extends Exception
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'Not found model exception';
    }
}

