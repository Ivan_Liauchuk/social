<?php

namespace common\helpers;

use Yii;

/**
 * Class AlertHelper
 * @package common\helpers
 */
class AlertHelper
{

    /**
     * @param string $type
     * @param string $message
     */
    private static function addMessage(string $type, string $message): void
    {
        Yii::$app->session->setFlash($type, $message);
    }

    /**
     * @param string $message
     */
    public static function success(string $message): void
    {
        self::addMessage('success', $message);
    }

    /**
     * @param string $message
     */
    public static function danger(string $message): void
    {
        self::addMessage('danger', $message);
    }

}