<?php

namespace common\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "appointments".
 *
 * @property int $id
 * @property string $date
 * @property int $time
 * @property string $name
 */
class Appointments extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'appointments';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['date', 'name', 'time'], 'required'],
            [['time'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'date' => 'Дата',
            'name' => 'Имя',
            'time' => 'Время'
        ];
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function beforeValidate()
    {
        $this->date = Yii::$app->formatter->asDate($this->date, 'php:Y-m-d');

        return parent::beforeValidate();
    }
}
