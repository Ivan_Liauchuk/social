<?php

namespace common\models;

use yii\data\ActiveDataProvider;

/**
 * Class DepartmentsSearch
 * @package common\models
 */
class DepartmentsSearch extends Departments
{
    /**
     * @return ActiveDataProvider
     */
    public function search(): ActiveDataProvider
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_ASC
                ]
            ],
        ]);

        return $dataProvider;
    }
}
