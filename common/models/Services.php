<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "services".
 *
 * @property integer $id
 * @property string $text
 */
class Services extends ActiveRecord
{
    private const DEFAULT_SERVICE_ID = 1;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['text'], 'required'],
            [['text'], 'string'],
        ];
    }

    /**
     * @return self
     */
    public static function getService(): self
    {

        if (($model = self::findOne(self::DEFAULT_SERVICE_ID)) !== null) {
            return $model;
        } else {
            $model = new self();
            $model->id = self::DEFAULT_SERVICE_ID;

            return $model;
        }
    }
}
