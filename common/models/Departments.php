<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "departments".
 *
 * @property int $id
 * @property string $title
 * @property string $info
 */
class Departments extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'departments';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['title', 'info'], 'required'],
            [['info'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'title' => 'Название',
            'info' => 'Информация',
        ];
    }
}
