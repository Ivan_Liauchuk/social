<?php

namespace common\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "complains".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $complain
 * @property string $create_date
 */
class Complains extends ActiveRecord
{

    public $isAnonymously = false;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'complains';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['complain', 'create_date'], 'required'],
            [['complain'], 'string'],
            [['create_date'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['phone'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Адрес электронной почты',
            'complain' => 'Жалоба',
            'isAnonymously' => 'Отправить анонимно',
        ];
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function beforeValidate(): bool
    {
        $this->create_date = Yii::$app->formatter->asDate(time(), 'php:Y-m-d');

        return parent::beforeValidate();
    }
}
