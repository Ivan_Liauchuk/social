<?php

namespace common\models;

use yii\data\ActiveDataProvider;

/**
 * Class NewsSearch
 * @package common\models
 */
class NewsSearch extends News
{

    private const LIMIT_FOR_SEARCH_FIELD = 10;

    /**
     * @return ActiveDataProvider
     */
    public function search(): ActiveDataProvider
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'update_date' => SORT_DESC
                ]
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @param string|null $text
     * @return ActiveDataProvider
     */
    public function searchByText(string $text = null): ActiveDataProvider
    {
        $query = self::find()
            ->andFilterWhere([
                'or',
                ['like', 'title', $text],
                ['like', 'text', $text],
            ])->limit(self::LIMIT_FOR_SEARCH_FIELD);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'update_date' => SORT_DESC
                ]
            ],
            'pagination' => false,
        ]);

        return $dataProvider;
    }
}
