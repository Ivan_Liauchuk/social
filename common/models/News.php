<?php

namespace common\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $update_date
 * @property string $create_date
 */
class News extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['title', 'text', 'update_date', 'create_date'], 'required'],
            [['text'], 'string'],
            [['update_date', 'create_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'title' => 'Заголовок',
            'text' => 'Содержание',
        ];
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function beforeValidate(): bool
    {
        $this->create_date = Yii::$app->formatter->asDate(time(), 'php:Y-m-d');
        $this->update_date = Yii::$app->formatter->asDate(time(), 'php:Y-m-d');

        return parent::beforeValidate();
    }
}
