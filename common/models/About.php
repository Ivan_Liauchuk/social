<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $address
 * @property string $info
 * @property string $phone
 * @property string $short_phone
 * @property string $email
 */
class About extends ActiveRecord
{

    private const DEFAULT_ID = 1;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'about';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['address', 'info'], 'required'],
            [['address', 'info',], 'string'],
            [['email'], 'email'],
            [['phone', 'short_phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'address' => 'Адрес',
            'info' => 'Дополнительная информация',
            'phone' => 'Телефон/факс',
            'short_phone' => 'Короткий номер телефона',
            'email' => 'Электронный адрес (e-mail)',
        ];
    }

    /**
     * @return About
     */
    public static function getModel(): self
    {
        $model = self::findOne(self::DEFAULT_ID);

        if ($model) {
            return $model;
        }

        $model = new self();
        $model->id = self::DEFAULT_ID;

        return $model;
    }
}
