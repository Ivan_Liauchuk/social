<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "documents".
 *
 * @property int $id
 * @property string $title
 * @property string $src
 * @property string $create_date
 * @property string $update_date
 */
class Documents extends ActiveRecord
{
    /** @var UploadedFile */
    public $uploadedFile;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'documents';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['title', 'create_date', 'update_date'], 'required'],
            [['uploadedFile'], 'file'],
            [['create_date', 'update_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function beforeValidate(): bool
    {
        $this->create_date = Yii::$app->formatter->asDate(time(), 'php:Y-m-d');
        $this->update_date = Yii::$app->formatter->asDate(time(), 'php:Y-m-d');

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'title' => 'Имя файла',
            'uploadedFile' => 'Выберите файл',
        ];
    }

    /**
     * @throws Exception
     * @return bool
     */
    public function upload(): bool
    {
        if ($this->validate()) {
            $fileName = Yii::$app->security->generateRandomString() . time() . '.' . $this->uploadedFile->extension;
            $this->uploadedFile->saveAs(Yii::getAlias('@documents') . '/' . $fileName);
            $this->src = $fileName;

            return true;
        }

        return false;
    }

    public function getUrl()
    {
        return Yii::getAlias('@documentsUrl') . '/' . $this->src;
    }
}
