<?php

namespace common\models;

use yii\data\ActiveDataProvider;

/**
 * Class DocumentsSearch
 * @package common\models
 */
class DocumentsSearch extends Documents
{
    /**
     * @return ActiveDataProvider
     */
    public function search(): ActiveDataProvider
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_ASC
                ]
            ]
        ]);

        return $dataProvider;
    }
}
