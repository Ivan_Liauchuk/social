<?php

namespace common\theme\assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;

/**
 * Class ThemeAsset
 * @package frontend\assets
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@common/theme/resources';

    public $css = [
        'libs/font-awesome/css/font-awesome.min.css',
        'libs/animate/animated.css',
        'libs/owl.carousel.min/owl.carousel.min.css',
        'libs/jquery.mmenu.all/jquery.mmenu.all.css',
        'libs/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
        'libs/direction/css/noJS.css',
        'libs/prettyphoto-master/css/prettyPhoto.css',
        'libs/slick-sider/slick.min.css',
        'libs/countdown-timer/css/demo.css',

        'css/home.css',
        'css/main.css',
        'css/custom.css',
    ];

    public $js = [
        'libs/animate/wow.min.js',
        'libs/jquery.mmenu.all/jquery.mmenu.all.min.js',
        'libs/countdown/jquery.countdown.min.js',
        'libs/jquery-appear/jquery.appear.min.js',
        'libs/jquery-countto/jquery.countTo.min.js',
        'libs/direction/js/jquery.hoverdir.js',
        'libs/direction/js/modernizr.custom.97074.js',
        'libs/isotope/isotope.pkgd.min.js',
        'libs/isotope/fit-columns.js',
        'libs/isotope/isotope-docs.min.js',
        'libs/mansory/mansory.js',
        'libs/prettyphoto-master/js/jquery.prettyPhoto.js',
        'libs/slick-sider/slick.min.js',
        'libs/countdown-timer/js/jquery.final-countdown.min.js',
        'libs/countdown-timer/js/kinetic.js',
        'libs/owl.carousel.min/owl.carousel.min.js',
        'js/main.js',
    ];

    public $depends = [
        BootstrapAsset::class,
    ];
}
