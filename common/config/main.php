<?php

use yii\caching\FileCache;
use yii\widgets\ListView;

return [
    'language' => 'ru',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@documents' => $_SERVER['DOCUMENT_ROOT'] . '/documents',
        '@documentsUrl' => '/documents',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
    ],
];
