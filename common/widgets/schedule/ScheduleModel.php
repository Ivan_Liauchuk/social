<?php

namespace common\widgets\schedule;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "schedule".
 *
 * @property int $id
 * @property string $schedule
 */
class ScheduleModel extends ActiveRecord
{
    public const MONDAY_KEY = 'Monday';
    public const TUESDAY_KEY = 'Tuesday';
    public const WEDNESDAY_KEY = 'Wednesday';
    public const THURSDAY_KEY = 'Thursday';
    public const FRIDAY_KEY = 'Friday';
    public const SATURDAY_KEY = 'Saturday';
    public const SUNDAY_KEY = 'Sunday';

    public const START_KEY = 'start';
    public const END_KEY = 'end';
    public const ENABLED_KEY = 'enabled';

    public const DEFAULT_START_VALUE = 8;
    public const DEFAULT_END_VALUE = 17;

    private const SCHEDULE_ID = 1;

    private const DAYS_OF_WEEK_LIST = [
        self::MONDAY_KEY,
        self::TUESDAY_KEY,
        self::WEDNESDAY_KEY,
        self::THURSDAY_KEY,
        self::FRIDAY_KEY,
        self::SATURDAY_KEY,
        self::SUNDAY_KEY,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'schedule';
    }

    /**
     * @return ScheduleModel
     */
    public static function getModel(): self
    {
        $model = self::findOne(self::SCHEDULE_ID);
        if ($model) {
            return $model;
        }

        $model = new self();
        $model->id = self::SCHEDULE_ID;

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'isEnabledSunday',
                    'startSunday',
                    'endSunday',
                    'isEnabledMonday',
                    'startMonday',
                    'endMonday',
                    'isEnabledTuesday',
                    'startTuesday',
                    'endTuesday',
                    'isEnabledWednesday',
                    'startWednesday',
                    'endWednesday',
                    'isEnabledThursday',
                    'startThursday',
                    'endThursday',
                    'isEnabledFriday',
                    'startFriday',
                    'endFriday',
                    'isEnabledSaturday',
                    'startSaturday',
                    'endSaturday',
                ],
                'required'
            ],
            [
                [
                    'isEnabledSunday',
                    'isEnabledMonday',
                    'isEnabledTuesday',
                    'isEnabledWednesday',
                    'isEnabledThursday',
                    'isEnabledFriday',
                    'isEnabledSaturday',
                ],
                'boolean'
            ],
            [
                [
                    'startSunday',
                    'endSunday',
                    'startMonday',
                    'endMonday',
                    'startTuesday',
                    'endTuesday',
                    'startWednesday',
                    'endWednesday',
                    'startThursday',
                    'endThursday',
                    'startFriday',
                    'endFriday',
                    'startSaturday',
                    'endSaturday',
                ],
                'integer'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'schedule' => 'Schedule',
        ];
    }

    /**
     * @return array
     */
    private function getJsonData(): array
    {
        return Json::decode($this->schedule, true);
    }

    /**
     * @param string $dayOfWeek
     * @return int
     */
    private function getStartValue(string $dayOfWeek)
    {
        return (int)ArrayHelper::getValue($this->getJsonData(), [
            $dayOfWeek,
            self::START_KEY
        ], self::DEFAULT_START_VALUE);
    }

    /**
     * @param string $dayOfWeek
     * @param int $value
     */
    private function setStartValue(string $dayOfWeek, int $value): void
    {
        $json = $this->getJsonData();

        $json[$dayOfWeek][self::START_KEY] = $value;

        $this->schedule = Json::encode($json, true);
    }

    /**
     * @param string $dayOfWeek
     * @param int $value
     */
    private function setEndValue(string $dayOfWeek, int $value): void
    {
        $json = $this->getJsonData();

        $json[$dayOfWeek][self::END_KEY] = $value;

        $this->schedule = Json::encode($json, true);
    }

    /**
     * @param string $dayOfWeek
     * @param bool $value
     */
    private function setIsEnabledValue(string $dayOfWeek, bool $value): void
    {
        $json = $this->getJsonData();

        $json[$dayOfWeek][self::ENABLED_KEY] = $value;

        $this->schedule = Json::encode($json, true);
    }

    /**
     * @param string $dayOfWeek
     * @return int
     */
    private function getEndValue(string $dayOfWeek): int
    {
        return (int)ArrayHelper::getValue($this->getJsonData(), [
            $dayOfWeek,
            self::END_KEY
        ], self::DEFAULT_END_VALUE);
    }

    /**
     * @param string $dayOfWeek
     * @return bool
     */
    private function getIsEnabled(string $dayOfWeek): bool
    {
        return (int)ArrayHelper::getValue($this->getJsonData(), [
            $dayOfWeek,
            self::ENABLED_KEY
        ], true);

    }

    /**
     * @return bool
     */
    public function getIsEnabledMonday(): bool
    {
        return $this->getIsEnabled(self::MONDAY_KEY);
    }

    /**
     * @param bool $value
     * @return void
     */
    public function setIsEnabledMonday(bool $value): void
    {
        $this->setIsEnabledValue(self::MONDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getStartMonday(): int
    {
        return $this->getStartValue(self::MONDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setStartMonday(int $value): void
    {
        $this->setStartValue(self::MONDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getEndMonday(): int
    {
        return $this->getEndValue(self::MONDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setEndMonday(int $value): void
    {
        $this->setEndValue(self::MONDAY_KEY, $value);
    }

    /**
     * @return bool
     */
    public function getIsEnabledTuesday(): bool
    {
        return $this->getIsEnabled(self::TUESDAY_KEY);
    }

    /**
     * @param bool $value
     * @return void
     */
    public function setIsEnabledTuesday(bool $value): void
    {
        $this->setIsEnabledValue(self::TUESDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getStartTuesday(): int
    {
        return $this->getStartValue(self::TUESDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setStartTuesday(int $value): void
    {
        $this->setStartValue(self::TUESDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getEndTuesday(): int
    {
        return $this->getEndValue(self::TUESDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setEndTuesday(int $value): void
    {
        $this->setEndValue(self::TUESDAY_KEY, $value);
    }

    /**
     * @return bool
     */
    public function getIsEnabledWednesday(): bool
    {
        return $this->getIsEnabled(self::WEDNESDAY_KEY);
    }

    /**
     * @param bool $value
     * @return void
     */
    public function setIsEnabledWednesday(bool $value): void
    {
        $this->setIsEnabledValue(self::WEDNESDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getStartWednesday(): int
    {
        return $this->getStartValue(self::WEDNESDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setStartWednesday(int $value): void
    {
        $this->setStartValue(self::WEDNESDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getEndWednesday(): int
    {
        return $this->getEndValue(self::WEDNESDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setEndWednesday(int $value): void
    {
        $this->setEndValue(self::WEDNESDAY_KEY, $value);
    }

    /**
     * @return bool
     */
    public function getIsEnabledThursday(): bool
    {
        return $this->getIsEnabled(self::THURSDAY_KEY);
    }

    /**
     * @param bool $value
     * @return void
     */
    public function setIsEnabledThursday(bool $value): void
    {
        $this->setIsEnabledValue(self::THURSDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getStartThursday(): int
    {
        return $this->getStartValue(self::THURSDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setStartThursday(int $value): void
    {
        $this->setStartValue(self::THURSDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getEndThursday(): int
    {
        return $this->getEndValue(self::THURSDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setEndThursday(int $value): void
    {
        $this->setEndValue(self::THURSDAY_KEY, $value);
    }

    /**
     * @return bool
     */
    public function getIsEnabledFriday(): bool
    {
        return $this->getIsEnabled(self::FRIDAY_KEY);
    }

    /**
     * @param bool $value
     * @return void
     */
    public function setIsEnabledFriday(bool $value): void
    {
        $this->setIsEnabledValue(self::FRIDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getStartFriday(): int
    {
        return $this->getStartValue(self::FRIDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setStartFriday(int $value): void
    {
        $this->setStartValue(self::FRIDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getEndFriday(): int
    {
        return $this->getEndValue(self::FRIDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setEndFriday(int $value): void
    {
        $this->setEndValue(self::FRIDAY_KEY, $value);
    }

    /**
     * @return bool
     */
    public function getIsEnabledSaturday(): bool
    {
        return $this->getIsEnabled(self::SATURDAY_KEY);
    }

    /**
     * @param bool $value
     * @return void
     */
    public function setIsEnabledSaturday(bool $value): void
    {
        $this->setIsEnabledValue(self::SATURDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getStartSaturday(): int
    {
        return $this->getStartValue(self::SATURDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setStartSaturday(int $value): void
    {
        $this->setStartValue(self::SATURDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getEndSaturday(): int
    {
        return $this->getEndValue(self::SATURDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setEndSaturday(int $value): void
    {
        $this->setEndValue(self::SATURDAY_KEY, $value);
    }

    /**
     * @return bool
     */
    public function getIsEnabledSunday(): bool
    {
        return $this->getIsEnabled(self::SUNDAY_KEY);
    }

    /**
     * @param bool $value
     * @return void
     */
    public function setIsEnabledSunday(bool $value): void
    {
        $this->setIsEnabledValue(self::SUNDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getStartSunday(): int
    {
        return $this->getStartValue(self::SUNDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setStartSunday(int $value): void
    {
        $this->setStartValue(self::SUNDAY_KEY, $value);
    }

    /**
     * @return int
     */
    public function getEndSunday(): int
    {
        return $this->getEndValue(self::SUNDAY_KEY);
    }

    /**
     * @param int $value
     * @return void
     */
    public function setEndSunday(int $value): void
    {
        $this->setEndValue(self::SUNDAY_KEY, $value);
    }

    /**
     * @param string $date
     * @return array
     */
    public function getWorkingHoursForDate(string $date): array
    {
        $dayOfWeekNumber = (int)date('w', strtotime($date));

        $dayOfWeekNumber = $dayOfWeekNumber - 1;
        if ($dayOfWeekNumber < 0) {
            $dayOfWeekNumber = 6;
        }

        $jsonKey = self::DAYS_OF_WEEK_LIST[$dayOfWeekNumber];

        if ($this->getIsEnabled($jsonKey)) {
            return [$this->getStartValue($jsonKey), $this->getEndValue($jsonKey)];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getDisabledDaysOfWeek(): array
    {
        $result = [];
        foreach (self::DAYS_OF_WEEK_LIST as $key => $dayOfWeek) {
            if (!$this->getIsEnabled($dayOfWeek)) {
                if ($key === 6) {
                    $result [] = 0;
                } else {
                    $result[] = $key + 1;
                }
            }
        }

        return $result;
    }
}
