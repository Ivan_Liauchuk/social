<?php

namespace common\widgets\schedule;

use Yii;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Class Schedule
 * @package common\widgets
 */
class Schedule extends Widget
{
    private const STEP = 1;

    /** @var ScheduleModel */
    public $model;

    /** @var ActiveForm */
    public $form;

    private const DAYS_OF_WEEK = [
        ScheduleModel::SUNDAY_KEY => 'Воскресенье',
        ScheduleModel::MONDAY_KEY => 'Понедельник',
        ScheduleModel::TUESDAY_KEY => 'Вторник',
        ScheduleModel::WEDNESDAY_KEY => 'Среда',
        ScheduleModel::THURSDAY_KEY => 'Четверг',
        ScheduleModel::FRIDAY_KEY => 'Пятница',
        ScheduleModel::SATURDAY_KEY => 'Суббота',
    ];

    /**
     * @param string $date
     * @param array $excludedTime
     * @return string
     */
    public static function getDropDownWithAvailableTime(string $date, array $excludedTime): string
    {

        $todayDate = date('Y-m-d');
        if (Yii::$app->formatter->asTimestamp($todayDate) > Yii::$app->formatter->asTimestamp($date)) {
            $items = [];
        } else {
            $model = ScheduleModel::getModel();
            $items = [];
            [$start, $end] = $model->getWorkingHoursForDate($date);
            for ($i = $start; $i < $end; $i += self::STEP) {
                $items[$i] = $i . ':00 - ' . ($i + self::STEP) . ':00';
            }

            $items = array_diff_key($items, array_flip($excludedTime));
        }

        return Html::dropDownList('test', null, $items);
    }

    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        $result = '';
        foreach (self::DAYS_OF_WEEK as $key => $dayOfWeek) {
            $result .= $this->renderDayItem($key, $dayOfWeek);
        }

        return $result;
    }

    /**
     * @param $selectedItem
     * @param $dayName
     * @return string
     */
    private function renderDayItem(string $selectedItem, string $dayName): string
    {
        $html = Html::activeCheckbox($this->model, 'isEnabled' . $selectedItem, [
                'label' => $dayName,
            ])
            .
            $this->renderTimeSelect('start', $selectedItem) .
            ' - ' .
            $this->renderTimeSelect('end', $selectedItem);

        return Html::tag('p', $html, ['class' => 'form-group']);
    }

    /**
     * @param string $name
     * @param null|string $selectedItem
     * @return string
     */
    private function renderTimeSelect(string $name, ?string $selectedItem): string
    {
        $items = [];

        $stopCondition = 24 - self::STEP;
        for ($i = 0; $i <= $stopCondition; $i++) {
            $items[] = ScheduleStringHelper::formatTime($i);
        }

        return Html::activeDropDownList($this->model, $name . $selectedItem, $items);
    }
}
