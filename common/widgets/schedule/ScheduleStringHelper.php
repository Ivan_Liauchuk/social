<?php

namespace common\widgets\schedule;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class ScheduleStringHelper
 * @package common\widgets\schedule
 */
class ScheduleStringHelper
{

    private $json;

    private const DAYS_OF_WEEK = [
        ScheduleModel::SUNDAY_KEY => 'Вс',
        ScheduleModel::MONDAY_KEY => 'Пн',
        ScheduleModel::TUESDAY_KEY => 'Вт',
        ScheduleModel::WEDNESDAY_KEY => 'Ср',
        ScheduleModel::THURSDAY_KEY => 'Чт',
        ScheduleModel::FRIDAY_KEY => 'Пт',
        ScheduleModel::SATURDAY_KEY => 'Сб',
    ];

    /**
     * ScheduleStringHelper constructor.
     * @param ScheduleModel $model
     */
    public function __construct(ScheduleModel $model)
    {
        $this->json = Json::decode($model->schedule);
    }

    /**
     * @return string
     */
    public function asString(): string
    {
        $enabledDaysOfWeek = $this->getEnabledDaysOfWeek();

        $groupedItems = $this->groupBySimilarDate($enabledDaysOfWeek);

        return $this->asHumanString(array_reverse($groupedItems));
    }

    /**
     * @param array $dateItems
     * @return string
     */
    private function asHumanString(array $dateItems)
    {
        $result = [];
        foreach ($dateItems as $time => $weekDays) {
            if (count($weekDays) > 2) {
                $rowItems = $this->getRowItems($weekDays);
                if (count($rowItems) > 1) {
                    $firstDay = reset($rowItems);
                    $lastDay = end($rowItems);
                    $rowItemsString = $firstDay . ' - ' . $lastDay;
                } else {
                    $rowItemsString = reset($rowItems);
                }
                $singleItems = array_diff($weekDays, $rowItems);;
                array_unshift($singleItems, $rowItemsString);
                $title = implode(', ', $singleItems);
            } elseif (count($weekDays) > 1) {
                $firstDay = reset($weekDays);
                $lastDay = end($weekDays);
                $title = $firstDay . ', ' . $lastDay;
            } else {
                $title = reset($weekDays);
            }
            $result[] = $title . ' ' . $time;
        }

        return implode('; ', $result);
    }

    /**
     * @return array
     */
    private function getEnabledDaysOfWeek(): array
    {
        $result = [];
        foreach (self::DAYS_OF_WEEK as $key => $shortName) {
            $dayOfWeek = ArrayHelper::getValue($this->json, $key);
            if ($this->isEnabledDayOfWeek($dayOfWeek)) {
                $result[$key] = $dayOfWeek;
            }
        }

        return $result;
    }

    /**
     * @param array $dayOfWeek
     * @return bool
     */
    private function isEnabledDayOfWeek(array $dayOfWeek): bool
    {
        return ArrayHelper::getValue($dayOfWeek, ScheduleModel::ENABLED_KEY, false);
    }

    /**
     * @param array $daysOfWeek
     * @return array
     */
    private function groupBySimilarDate(array $daysOfWeek): array
    {
        $mainResult = [];
        $firstItem = reset($daysOfWeek);
        $firstItemKey = key($daysOfWeek);
        $result = [$this->getShortName($firstItemKey)];
        unset($daysOfWeek[$firstItemKey]);

        foreach ($daysOfWeek as $key => $nexDay) {
            if (
                $this->getStartTime($firstItem) === $this->getStartTime($nexDay) &&
                $this->getEndTime($firstItem) === $this->getEndTime($nexDay)
            ) {
                $result[] = $this->getShortName($key);
                unset($daysOfWeek[$key]);
            }
        }

        if (!empty($daysOfWeek)) {
            $mainResult += $this->groupBySimilarDate($daysOfWeek);
        }

        $resultKey = $this->generateTimeInterval($this->getStartTime($firstItem), $this->getEndTime($firstItem));
        $mainResult[$resultKey] = $result;

        return $mainResult;
    }

    /**
     * @param array $dayOfWeek
     * @return int
     */
    private function getStartTime(array $dayOfWeek): int
    {
        return $dayOfWeek[ScheduleModel::START_KEY];
    }

    /**
     * @param array $dayOfWeek
     * @return int
     */
    private function getEndTime(array $dayOfWeek): int
    {
        return $dayOfWeek[ScheduleModel::END_KEY];
    }

    /**
     * @param string $key
     * @return string
     */
    private function getShortName(string $key): string
    {
        return self::DAYS_OF_WEEK[$key];
    }

    /**
     * @param array $weekDays
     * @return array
     */
    private function getRowItems(array $weekDays): array
    {
        $firstItem = reset($weekDays);

        $result = [$firstItem];
        unset($weekDays[key($weekDays)]);

        $indexedArray = array_values(self::DAYS_OF_WEEK);
        $itemIndex = array_search($firstItem, $indexedArray) + 1;

        $countOfElements = count(self::DAYS_OF_WEEK);

        while ($itemIndex < $countOfElements) {
            $nextItem = $indexedArray[$itemIndex];
            if (in_array($nextItem, $weekDays)) {
                $result[] = $nextItem;
            } else {
                break;
            }
            $itemIndex++;
        }

        return $result;
    }


    /**
     * @param int $startTime
     * @param int $endTime
     * @return string
     */
    public static function generateTimeInterval(int $startTime, int $endTime): string
    {
        return implode(' - ', [self::formatTime($startTime), self::formatTime($endTime)]);
    }


    /**
     * @param int $hour
     * @return string
     */
    public static function formatTime(int $hour): string
    {
        $hour =  strlen($hour) > 1 ? $hour : '0' . $hour;

        return $hour . ':00';
    }
}