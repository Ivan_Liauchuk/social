<?php

use common\widgets\schedule\Schedule;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Время работы';
?>

<div class="about-form">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= Schedule::widget([
        'form' => $form,
        'model' => $model
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>