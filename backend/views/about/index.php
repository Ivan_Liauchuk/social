<?php

use common\widgets\schedule\Schedule;
use vova07\imperavi\Widget;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'О нас';
?>

<div class="about-form">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'phone')->textInput() ?>
    <?= $form->field($model, 'short_phone')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'info')->widget(Widget::class) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
