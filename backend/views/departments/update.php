<?php

use common\models\Departments;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Departments
 */

$this->title = 'Обновить отделение';
?>
<div class="departments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
