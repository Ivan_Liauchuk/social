<?php

use common\models\Departments;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;


/**
 * @var $this View
 * @var $model Departments
 * @var $form ActiveForm
 */
?>

<div class="departments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info')->widget(Widget::class) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Назад', ['departments/index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
