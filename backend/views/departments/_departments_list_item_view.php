<?php

use common\models\Departments;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Departments
 */

?>
<div class="row mb-20">

    <div class="col-md-10">
        <?= Html::a($model->title, [
            'departments/update',
            'id' => $model->id
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::a('Изменить', [
            'departments/update',
            'id' => $model->id
        ], [
            'class' => 'btn btn-sm btn-primary'
        ]) ?>
        <br/>
        <?= Html::a('Удалить', [
            'departments/delete',
            'id' => $model->id
        ], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'method' => 'post',
            ]
        ]) ?>
    </div>
</div>