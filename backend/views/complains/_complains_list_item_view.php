<?php

use common\models\Complains;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Complains
 */

?>
<div class="row mb-20">

    <div class="col-md-2">
        <?= $model->name ? $model->name : 'Анонимно' ?>
    </div>
    <div class="col-md-2">
        <div class="col-md-12">
            <?= $model->phone ? $model->phone : null ?>
        </div>
        <div class="col-md-12">
            <?= $model->email ? $model->email : null ?>
        </div>
    </div>
    <div class="col-md-4">
        <?= $model->complain ?>
    </div>
    <div class="col-md-2">
        <?= Yii::$app->formatter->asDate($model->create_date) ?>
    </div>
    <div class="col-md-2">
        <?= Html::a('Удалить', [
            'complains/delete',
            'id' => $model->id
        ], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'method' => 'post',
            ]
        ]) ?>
    </div>
</div>