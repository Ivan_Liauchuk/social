<?php

use common\models\Documents;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Documents
 */

?>
<div class="row mb-20">

    <div class="col-md-10">
        <?= Html::a($model->title, $model->getUrl(), [
            'download' => true,
            '_target' => 'blank',
        ]) ?>
        <br/>
    </div>
    <div class="col-md-2">
        <?= Html::a('Изменить', [
            'documents/update',
            'id' => $model->id
        ], [
            'class' => 'btn btn-sm btn-primary'
        ]) ?>
        <br/>
        <?= Html::a('Удалить', [
            'documents/delete',
            'id' => $model->id
        ], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'method' => 'post',
            ]
        ]) ?>
    </div>
</div>