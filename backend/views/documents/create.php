<?php

use common\models\Documents;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model Documents
 */

$this->title = 'Добавить документ';
?>
<div class="documents-create">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
