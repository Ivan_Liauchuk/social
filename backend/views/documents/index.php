<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var $this View
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Правовая база';
?>
<div class="news-index">

    <h1 class="text-center">
        <?= Html::encode($this->title) ?></h1>

    <p class="mt-20 mb-20">
        <?= Html::a('Добавить документ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_documents_list_item_view',
        'layout' => "{items}\n{pager}"
    ]); ?>
</div>
