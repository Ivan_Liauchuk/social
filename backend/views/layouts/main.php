<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\Nav;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="/img/logo.png" type="image/x-icon">

    <!-- Web Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Pacifico%7CSource+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;amp;subset=latin-ext,vietnamese"
          rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    -->
    <?php $this->head() ?>
</head>
<body class="home tree-shop-home has-header-sidebar">
<?php $this->beginBody() ?>
<div id="preloaderKDZ"></div>
<div class="yolo-site">


    <header class="header yolo-header-style-4">
        <div class="yolo-top-bar">
            <div class="container">
                <div class="top-sidebar text-center">
                    Центр социального обслуживания населения
                </div>
            </div>
        </div>


        <div class="mobile-menu">
            <div class="col-3 text-left">
                <a href="#primary-menu">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            <div class="col-3 text-center">
                <div class="logo">
                    <h1>
                        <a href="index.html">
                            <img src="/img/logo.png" alt="logo">
                        </a>
                    </h1>
                </div>
            </div>
        </div>
        <div class="header-sidebar">

            <div class="header-bottom">
                <div class="main-nav-wrapper">
                    <div class="header-left">
                        <nav id="primary-menu" class="main-nav">
                            <?php
                            $menuItems = [
                                [
                                    'label' => 'Новости',
                                    'url' => ['/'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Услуги',
                                    'url' => ['services/index'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Правовая база',
                                    'url' => ['documents/index'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'О нас',
                                    'url' => ['about/index'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Вермя работы',
                                    'url' => ['about/schedule'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Жалобы',
                                    'url' => ['complains/index'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Обращения граждан',
                                    'url' => ['appointments/index'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Структура центра',
                                    'url' => ['departments/index'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                ],
                                [
                                    'label' => 'Выйти',
                                    'url' => ['site/logout'],
                                    'options' => [
                                        'class' => 'menu-item',
                                    ],
                                    'linkOptions' => [
                                        'data' => [
                                            'method' => 'post',
                                        ]
                                    ]
                                ],

                            ];

                            echo Nav::widget([
                                'options' => ['class' => 'nav'],
                                'items' => $menuItems,
                            ]);
                            ?>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </header>

    <div id="wrapper">
        <div class="container mt mb">
            <?= Alert::widget() ?>

            <?= $content ?>
        </div>
    </div>
</div>

<div class="popup-wrapper"></div>

<div class="click-back-top-body">
    <button type="button" class="sn-btn sn-btn-style-17 sn-back-to-top fixed-right-bottom">
        <i class="btn-icon fa fa-angle-up"></i>
    </button>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
