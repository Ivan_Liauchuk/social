<?php

use common\widgets\schedule\Schedule;
use common\widgets\schedule\ScheduleStringHelper;
use yii\helpers\Html;
use yii\web\View;
use common\models\Appointments;

/**
 * @var $this View
 * @var $model Appointments
 */

?>
<div class="row mb-20">

    <div class="col-md-2">
        <?= Yii::$app->formatter->asDate($model->date, 'php: d.m.Y') . ' ' .
        ScheduleStringHelper::formatTime($model->time) ?>
    </div>
    <div class="col-md-8">
        <?= $model->name ?>
    </div>
    <div class="col-md-2">
        <?= Html::a('Удалить', [
            'appointments/delete',
            'id' => $model->id
        ], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'method' => 'post',
            ]
        ]) ?>
    </div>
</div>