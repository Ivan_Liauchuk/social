<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var $this View
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Обращения граждан';
?>
<div class="appointments-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ListView::widget([
        'layout' => "{items}\n{pager}",
        'dataProvider' => $dataProvider,
        'itemView' => '_appointments_list_item_view',
    ]); ?>
</div>
