<?php

use common\models\News;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $model News
 */

?>
<div class="row mb-20">

    <div class="col-md-10">
        <b class="mb-20">
            <?= Html::a($model->title, [
                'news/update',
                'id' => $model->id
            ]) ?>
        </b>
        <div>
            <?= StringHelper::truncate($model->text, 400) ?>
        </div>
    </div>
    <div class="col-md-2">
        <?= Html::a('Изменить', [
            'news/update',
            'id' => $model->id
        ], [
            'class' => 'btn btn-sm btn-primary'
        ]) ?>
        <br/>
        <?= Html::a('Удалить', [
            'news/delete',
            'id' => $model->id
        ], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'method' => 'post',
            ]
        ]) ?>
    </div>
</div>