<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var $this View
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Новости';
?>
<div class="news-index">

    <h1 class="text-center mb-20">
        <?= Html::encode($this->title) ?>
    </h1>

    <p class="mb-20">
        <?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_news_list_item_view',
        'layout' => "{items}\n{pager}"
    ]); ?>
</div>
