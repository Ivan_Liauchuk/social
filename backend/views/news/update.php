<?php

use common\models\News;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model News
 */

$this->title = 'Обновить новость: ' . $model->title;
?>
<div class="news-update">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
