<?php

namespace backend\controllers;

use common\helpers\AlertHelper;
use common\models\Services;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ServicesController
 * @package backend\controllers
 */
class ServicesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|Response
     */
    public function actionIndex()
    {
        $model = Services::getService();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                AlertHelper::success('Данные успешно обновлены');

                return $this->redirect(['services/index']);
            }
            AlertHelper::danger('Произошла ошибка');
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
