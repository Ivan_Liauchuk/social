<?php

namespace backend\controllers;

use common\helpers\AlertHelper;
use common\models\About;
use common\widgets\schedule\ScheduleModel;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class AboutController
 * @package backend\controllers
 */
class AboutController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all About models.
     * @return string|Response
     */
    public function actionIndex()
    {
        $model = About::getModel();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                AlertHelper::success('Данные успешно обновлены');
                return $this->redirect(['about/index']);
            } else {
                AlertHelper::danger('При обновлении произошла ошибка');
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionSchedule()
    {
        $model = ScheduleModel::getModel();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                AlertHelper::success('Данные успешно обновлены');

                return $this->redirect(['about/schedule']);
            } else {
                AlertHelper::danger('При обновлении произошла ошибка');
            }
        }

        return $this->render('schedule', [
            'model' => $model,
        ]);
    }
}
