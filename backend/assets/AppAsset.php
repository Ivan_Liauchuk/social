<?php

namespace backend\assets;

use common\theme\assets\ThemeAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class AppAsset
 * @package backend\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [

    ];
    public $depends = [
        YiiAsset::class,
        ThemeAsset::class
    ];
}
