-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: social.test    Database: social
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about`
--

LOCK TABLES `about` WRITE;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` VALUES (1,'Минская обл.,\r\n г. Любань\r\nул.Ленина, 8','8(0174)211963','170','tsoid@yandex.ru','<p><strong>Отделение первичного приема, анализа и прогнозирования.</strong><br></p><p> Телефон:  8-(0174)-22-95-56</p>  <p><strong>Отделение срочного социального обслуживания.</strong><br> Телефоны: 8-(0174)-22-66 -07, 8-(0174)-22-40 -05</p>  <p><strong>Отделение социальной адаптации и реабилитации.</strong><br> Телефон: 8-(0174)-22-93-66. Телефон \"доверия\" - 170</p>  <p><strong>Отделение дневного пребывания для инвалидов.</strong><br> Телефон: 8-(0174)-26-05-79</p>  <p><strong>Отделение социальной помощи на дому.</strong><br> Телефоны:  8-(0174)-28-77-09, 8-(0174)-28-80-05</p>  <p><strong>Отделение дневного пребывания для граждан пожилого возраста.</strong><br> Телефон:  8-(0174)-22-56-11</p>  <p><strong>Отделение сопровождаемого проживания.</strong><br> Телефон:  8-(0174)-22-93-66</p>  <p><strong>Отделение дневного пребывания для граждан пожилого возраста с предоставлением услуги по уходу</strong></p>  <p>Телефон: 8 029-6843554</p>  <p><strong>Телефоны \"доверия\"</strong>:</p>  <p>короткий номер 170 или 8-(0174)-22-93-66.</p>');
/*!40000 ALTER TABLE `about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (5,'Иван','2017-12-28',10),(6,'Иван','2017-12-28',8),(7,'Иван','2017-12-28',9),(8,'Иван','2017-12-28',18);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complains`
--

DROP TABLE IF EXISTS `complains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complain` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complains`
--

LOCK TABLES `complains` WRITE;
/*!40000 ALTER TABLE `complains` DISABLE KEYS */;
INSERT INTO `complains` VALUES (1,'','','','asdasdasd','2017-12-25 00:00:00'),(2,'','','','Тест','2017-12-25 00:00:00'),(3,'','','','Все плохо','2017-12-25 00:00:00'),(4,'','','','Анонимная жалоба','2017-12-26 00:00:00');
/*!40000 ALTER TABLE `complains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'ОТДЕЛЕНИЕ СОЦИАЛЬНОЙ ПОМОЩИ НА ДОМУ','<p>Заведующий отделением<strong>– Абчинец Татьяна Анатольевна </strong></p><p> <strong>Тел. 61-8-98, 55-2-47, 61-0-67</strong></p>  <p><strong>Цель работы отделения</strong> - оказание нетрудоспособным гражданам, частично сохранивших способность к передвижению и самообслуживанию, консультационно - информационных, социально – бытовых, социально – медицинских, социально - педагогических, социально - посреднических и социально – реабилитационных услуг.</p>  <p><strong>Организационная деятельность</strong><br> Работает « Школа социального работника» (1 раз в месяц).<br> Отделение обслуживает на дому 1090 человек.<br> Инициирует проведение мероприятий в домах социальных услуг (16).<br> Организована работа 8 социальных пунктов на базе сельсоветов.<br> Работает 10 бригад на мобильной основе.</p>  <p><strong>Ежегодно проводятся конкурсы:</strong><br> «Социальный работник года»;<br> «Ненаглядная сторона».</p>  <p><strong>Инновационные технологии социального обслуживания</strong><br> «Санаторий на дому»<br> «Дом зимовки»<br> «Патронатная семья»<br> «Гостевая семья»</p>  <p><strong>Социальные проекты</strong><br> «Мы за здоровье семьи и здоровое поколение»<br> «Цветник у каждого дома обслуживаемого»</p>'),(2,'ОТДЕЛЕНИЕ СОЦИАЛЬНОЙ АДАПТАЦИИ И РЕАБИЛИТАЦИИ','<p><strong>Заведующий отделением<i> – </i>Жартун Оксана Викторовна</strong></p>  <p>Отделение социальной адаптации и реабилитации оказывает содействие в восстановлении способности к жизнедеятельности в социальной среде гражданам (семьям), находящимся в трудной жизненной ситуации, через предоставление консультационно-информационных, социально-педагогических, посреднических, реабилитационных услуг, услуг временного приюта и сопровождаемого проживания.</p>  <p><i>Трудная жизненная ситуация – ситуация, объективно нарушающая нормальную жизнедеятельность гражданина и сложная для его самостоятельного разрешения.</i></p>  <p><u>Социальные услуги оказываются в форме нестационарного обслуживания.</u></p>  <p><strong><i><u>Консультационно-информационные услуги:</u></i></strong><strong></strong></p>  <p>-информирование граждан о перечне социальных услуг, оказываемых территориальными центрами социального обслуживания населения (юридическая, психологическая, информационная помощь и т.д.);<br> <strong>-</strong>оказание помощи в написании заявлений, обращений, оформлении документов по вопросам социального обслуживания, социальной помощи, социальных выплат и льгот;<br> -содействие в истребовании необходимых документов для получения предусмотренных законодательством льгот, гарантий и их представлении в соответствующие органы для рассмотрения; <br> -проведение консультаций и предоставление необходимой информации по вопросам социального обслуживания, социальной помощи, социальных выплат и льгот; <br> -проведение информационных бесед, встреч.</p>  <p><strong><i><u>Социальный патронат:</u></i></strong><strong></strong></p>  <p>-содействие в жизнеустройстве граждан (семей), находящихся в трудной жизненной ситуации (получение и оформление всех видов льгот и гарантий, предусмотренных законодательством).<br></p><p>-психологическое консультирование.</p>');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `src` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (2,'Директива Президента Республики Беларусь от 27 декабря 2006 г. № 2 \"О мерах по дальнейшей дебюрократизации государственного аппарата\"','arjMv5UGf3ObgfCP7On028q1vLVdE3-d1514151680.txt','2017-12-24 00:00:00','2017-12-24 00:00:00');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_date` datetime NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (2,'Кто относится к нетрудоспособным одиноким и одиноко проживающим гражданам','<p>Кто относится к нетрудоспособным одиноким и одиноко проживающим гражданам</p><p>Вопрос. Кто относится к нетрудоспособным одиноким и одиноко проживающим гражданам?</p><p>Ответ. </p><p>Нетрудоспособный одинокий гражданин - нетрудоспособный гражданин, не имеющий трудоспособных членов семьи, обязанных по закону его содержать.</p><p>Нетрудоспособный одиноко проживающий гражданин - нетрудоспособный гражданин, проживающий отдельно от трудоспособных членов семьи, обязанных по закону его содержать.</p><p>В свою очередь, нетрудоспособным является гражданин, достигший возраста, дающего право на пенсию по возрасту на общих основаниях (женщина 55 лет и старше, мужчина 60 лет и старше), инвалид I или II группы, ребенок в возрасте до 18 лет.</p><p>В соответствии с Кодексом Республики Беларусь о браке и семье членами семьи, обязанными по закону содержать нетрудоспособного гражданина, являются трудоспособные дети, родители, супруг (супруга).</p><p>Вопрос. Какова периодичность посещения обслуживаемых граждан социальными работниками?</p><p>Ответ. </p><p>Периодичность посещения обслуживаемых граждан социальными работниками зависит от группы двигательной активности и вида оплаты за обслуживание. Группа снижения двигательной активности определяется организацией здравоохранения. В зависимости от двигательной активности и способности к самообслуживанию граждане делятся на группы снижения двигательной активности.</p><p>Периодичность посещения нетрудоспособных граждан социальными работниками устанавливается на основании договора, определяющего количество визитов, виды и объем предоставляемых социальных услуг, а также  на основании  медицинской  справки:</p><p>с умеренно сниженной двигательной активностью – 2 раза в неделю;</p><p>со значительно сниженной двигательной активностью – 3 раза в неделю;</p><p>утративших двигательную активность, – 4-5 раз в неделю.</p>','2017-12-24 12:00:35','2017-12-24 12:00:37');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES (1,'{\"Friday\": {\"end\": 17, \"start\": 8, \"enabled\": true}, \"Monday\": {\"end\": 17, \"start\": 8, \"enabled\": true}, \"Sunday\": {\"end\": 21, \"start\": 13, \"enabled\": false}, \"Tuesday\": {\"end\": 19, \"start\": 8, \"enabled\": true}, \"Saturday\": {\"end\": 13, \"start\": 9, \"enabled\": true}, \"Thursday\": {\"end\": 19, \"start\": 8, \"enabled\": true}, \"Wednesday\": {\"end\": 17, \"start\": 8, \"enabled\": true}}');
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'<p>ГУ \"Любанский районный территориальный центр социального обслуживания населения\" оказывает услуги согласно перечню бесплатных и общедоступных социальных услуг государственных учреждений социального обслуживания</p><p><br></p>        <p><a href=\"http://social-soligorsk.by/images/norm-baza/o-nekotoryh-voprosah-okazaniya-socialnyh-uslug-1218.doc\">Постановление совета министров от 27 декабря 2012 г. №1218 \" О некоторых вопросах оказания социальных услуг\"</a></p><p><br></p>        <p>с 1 октября 2015г Центром оказываются дополнительно разовые социальные услуги на платной основе:</p><p><br></p>      <p style=\"margin-left: 20px;\">- приготовление пищи;</p>  <p style=\"margin-left: 20px;\">- консервирование плодоовощной продукции;</p>  <p style=\"margin-left: 20px;\">- уход за комнатными растениями;</p>  <p style=\"margin-left: 20px;\">- мытье посуды;</p>  <p style=\"margin-left: 20px;\">- мытье кухонных шкафчиков;</p>  <p style=\"margin-left: 20px;\">- мытье дверей;</p>  <p style=\"margin-left: 20px;\">- разморозка холодильника;</p>  <p style=\"margin-left: 20px;\">- стирка белья;</p>  <p style=\"margin-left: 20px;\">- глажка белья;</p>  <p style=\"margin-left: 20px;\">- чистка унитаза;</p>  <p style=\"margin-left: 20px;\">- колка дров;</p>  <p style=\"margin-left: 20px;\">- распиловка дровяного долготья на заданную длину механизированной пилой;</p>  <p style=\"margin-left: 20px;\">- укладка колотых дров;</p>  <p style=\"margin-left: 20px;\">- переноска торфяного брикета;</p>  <p style=\"margin-left: 20px;\">- обработка садовых деревьев бензопилой;</p>  <p style=\"margin-left: 20px;\">- прополка огорода;</p>  <p style=\"margin-left: 20px;\">-косьба травы с помощью триммера;</p>  <p style=\"margin-left: 20px;\">- обработка приусадебного участка с помощью мотоблока;</p>  <p style=\"margin-left: 20px;\">- малярные работы;</p>  <p style=\"margin-left: 20px;\">- обойные работы;</p>  <p style=\"margin-left: 20px;\">- другие ремонтно-строительные услуги;</p>  <p style=\"margin-left: 20px;\">- посещение в больнице обслуживаемых граждан;</p>  <p style=\"margin-left: 20px;\">- «Гостиница выходного дня» - услуга по социальной передышке для родителей, воспитывающих детей-инвалидов и молодых инвалидов;</p>  <p style=\"margin-left: 20px;\">- ремонт швейных изделий;</p>  <p style=\"margin-left: 20px;\">- платные семинары в организациях Люанского района;</p>  <p style=\"margin-left: 20px;\">- поздравление Деда Мороза и Снегурочки;</p>  <p style=\"margin-left: 20px;\">- обучение оздоровительной гимнастике «Цигун».</p>');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin user','admin','$2y$13$V9/wa6CROmfDiRgDGyCEpulidsGO58wVMV.OBZnWhUDm.z/U4wu5O');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-27 16:12:33
